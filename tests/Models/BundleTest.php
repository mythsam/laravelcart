<?php
namespace Models;

use App\Models\Bundle;
use App\Models\Product;
use TestCase;

/**
 * @covers App\Models\Bundle
 */
class BundleTest extends TestCase
{
    /**
     * @var array
     */
    private $expectedData = [
        'name' => 'bundle name',
        'active' => 1,
        'discount' => 20
    ];

    /**
     * @var Bundle
     */
    private $SUT;

    public function setUp()
    {
        parent::setUp();

        $this->SUT = Bundle::create($this->expectedData);
    }

    public function test_it_should_create_a_bundle_with_expected_data()
    {
        $this->assertSame($this->expectedData['name'], $this->SUT->name);
        $this->assertSame($this->expectedData['active'], $this->SUT->active);
        $this->assertSame($this->expectedData['discount'], $this->SUT->discount);
    }

    public function test_bundle_should_have_list_of_products()
    {
        $amount = 2;
        $this->attachRandomProducts($amount);

        $this->assertSame($amount, count($this->SUT->products()->get()->lists('id')));
    }

    public function test_it_should_get_products_as_array()
    {
        $this->attachRandomProducts(2);

        $bundle = Bundle::where('name', $this->expectedData['name'])->find(1);

        $this->assertSame([1, 2], $bundle->getProductListAttribute());
    }

    /**
     * @param int $amount
     */
    private function attachRandomProducts($amount)
    {
        factory(Product::class, $amount)
            ->create()
            ->each(
                function ($product) {
                    $this->SUT->products()->attach($product);
                }
            );
    }
}