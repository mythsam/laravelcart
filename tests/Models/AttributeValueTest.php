<?php
namespace Models;

use App\Models\AttributeValue;
use App\Models\Attribute;
use TestCase;

/**
 * @covers App\Models\AttributeValue
 */
class AttributeValueTest extends TestCase
{
    /**
     * @var array
     */
    private $expectedData = [
        'value' => 'Blue'
    ];

    /**
     * @var AttributeValue
     */
    private $SUT;

    public function setUp()
    {
        parent::setUp();

        $attribute = Attribute::create(['name' => 'Color']);
        $this->expectedData['attribute_id'] = $attribute->id;

        $this->SUT = AttributeValue::create($this->expectedData);
    }

    public function test_it_should_create_an_attribute_value_with_expected_data()
    {
        $this->assertSame($this->expectedData['attribute_id'], $this->SUT->attribute_id);
        $this->assertSame($this->expectedData['value'], $this->SUT->value);
    }

    public function test_attribute_value_should_belong_to_attribute()
    {
        $this->assertSame([$this->expectedData['attribute_id']], $this->SUT->attribute()->get()->lists('id')->toArray());
    }
}