<?php
namespace Models;

use App\Models\Product;
use App\Models\Attribute;
use App\Models\Bundle;
use TestCase;

/**
 * @covers App\Models\Product
 */
class ProductTest extends TestCase
{
    /**
     * @var array
     */
    private $expectedData = [
        'name' => 'product name',
        'description' => 'product description',
        'active' => 1,
        'price' => 1.25
    ];

    /**
     * @var Product
     */
    private $SUT;

    public function setUp()
    {
        parent::setUp();

        $this->SUT = Product::create($this->expectedData);
    }

    public function test_it_should_create_a_product_with_expected_data()
    {
        $this->assertSame($this->expectedData['name'], $this->SUT->name);
        $this->assertSame($this->expectedData['description'], $this->SUT->description);
        $this->assertSame($this->expectedData['active'], $this->SUT->active);
        $this->assertSame($this->expectedData['price'], $this->SUT->price);
    }

    public function test_product_should_have_list_of_attributes()
    {
        $amount = 2;
        $this->attachRandomAttributes($amount);

        $this->assertSame($amount, count($this->SUT->attributes()->get()->lists('id')));
    }

    public function test_product_should_link_with_bundles()
    {
        $amount = 3;
        $this->attachRandomBundles($amount);

        $this->assertSame($amount, count($this->SUT->bundles()->get()->lists('id')));
    }

    public function test_it_should_get_attributes_as_array()
    {
        $this->attachRandomAttributes(2);

        $product = Product::where('name', $this->expectedData['name'])->find(1);

        $this->assertSame([1, 2], $product->getAttributeListAttribute());
    }

    /**
     * @param int $amount
     */
    private function attachRandomAttributes($amount)
    {
        factory(Attribute::class, $amount)
            ->create()
            ->each(
                function ($attribute) {
                    $this->SUT->attributes()->attach($attribute);
                }
            );
    }

    /**
     * @param int $amount
     */
    private function attachRandomBundles($amount)
    {
        factory(Bundle::class, $amount)
            ->create()
            ->each(
                function ($bundle) {
                    $this->SUT->bundles()->attach($bundle);
                }
            );
    }
}