<?php
namespace Models;

use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Product;
use TestCase;

/**
 * @covers App\Models\Attribute
 */
class AttributeTest extends TestCase
{
    const NAME_COLOR = 'Color';

    public function setUp()
    {
        parent::setUp();

        $SUT = Attribute::create(['name' => self::NAME_COLOR]);

        AttributeValue::create(['attribute_id'=>$SUT->id, 'value' => 'Blue']);
        AttributeValue::create(['attribute_id'=>$SUT->id, 'value' => 'Red']);
        AttributeValue::create(['attribute_id'=>$SUT->id, 'value' => 'Yellow']);

        factory(Product::class, 2)
            ->create()
            ->each(function($attributes) use ($SUT) {
                $attributes->attributes()->save($SUT);
            });
    }

    public function test_it_should_create_a_attribute()
    {
        $attribute = Attribute::find(1);

        $this->assertSame(self::NAME_COLOR, $attribute->name);
    }

    public function test_attribute_should_have_values()
    {
        $attribute = Attribute::where('name', self::NAME_COLOR)->find(1);

        $this->assertSame(3, count($attribute->values()->get()->lists('id')));
    }

    public function test_attribute_should_link_with_products()
    {
        $attribute = Attribute::where('name', self::NAME_COLOR)->find(1);

        $this->assertSame(2, count($attribute->products()->get()->lists('id')));
    }

    public function test_it_should_get_values_as_array()
    {
        $attribute = Attribute::where('name', self::NAME_COLOR)->find(1);

        $this->assertSame([1,2,3], $attribute->getValueListAttribute());
    }
}