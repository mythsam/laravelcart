<?php

use App\Models\Attribute;
use App\Models\Product;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ProductControllerTest extends TestCase
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Attribute
     */
    protected $attribute;

    public function setUp()
    {
        parent::setUp();
    }

    public function test_it_should_land_to_product_home_page()
    {
        $this->call('GET', '/products');

        $this->see('Product List');
    }

    public function test_it_should_show_product_list()
    {
        factory(Product::class, 2)->create();

        $response = $this->call('GET', '/products');

        $view = $response->original;

        $this->assertResponseOk();
        $this->assertViewHas('products');
        $this->assertSame(2, count($view['products']));
    }

    public function test_it_should_show_product_view()
    {
        factory(Product::class, 1)->create();

        $this->call('GET', '/products/1');

        $this->assertResponseOk();
        $this->assertViewHas('product');
        $this->assertViewHas('bundles');
        $this->see('Add To Cart');
        $this->see('Price');
        $this->see('Available Options');
    }

    /**
     * @dataProvider provider_for_test_protected_area_should_not_be_accessed
     */
    public function test_protected_area_should_not_be_accessed($method, $url)
    {
        factory(Product::class)->create();

        $response = $this->call($method, $url);
        $this->assertSame(302, $response->getStatusCode());
        $this->assertRedirectedTo('auth/login');
    }

    public function provider_for_test_protected_area_should_not_be_accessed()
    {
        return [
            ['GET', '/products/create'],
            ['GET', '/products/manage'],
            ['GET', '/products/1/edit'],
        ];
    }
}
