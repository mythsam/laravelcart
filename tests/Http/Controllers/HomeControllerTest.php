<?php


class HomeControllerTest extends TestCase
{
    public function test_landing_home_page()
    {
        $this->visit('/');

        $this->see('Welcome to LaraCart');
    }

    public function test_home_page_can_click_to_products()
    {
        $this->visit('/');

        $this->click('click here');

        $this->seePageIs('/products');
    }
}
