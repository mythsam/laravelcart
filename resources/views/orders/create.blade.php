@extends('app')

@section('content')
    <h3>Order received.</h3>

    <p>
        Confirmation email sent to {{$user['email']}}. Thank you {{$user['name']}}!
    </p>

    <p>Please <a href="/products">Click Here</a> to continue shopping. </p>
@endsection