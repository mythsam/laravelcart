@extends('app')

@section('content')
    <h1>Ordering</h1>

    <hr>
    @if(count($cart))
        {!! Form::open(['url' => 'order', 'method' => 'POST', 'action'=>['OrderController@create']]) !!}
        <section id="order_items">
            <div class="container">
                <div class="table-responsive order_info">

                        <table class="table table-condensed">
                            <thead>
                            <tr class="order_menu">
                                <td class="title">Item</td>
                                <td class="description"></td>
                                <td class="price">Price</td>
                                <td class="quantity">Quantity</td>
                                <td class="total">Total</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($cart as $rowId => $item)
                                <tr>
                                    <td class="order_product">
                                        <h4><a href="products/{{$item->id}}">{{$item->name}}</a></h4>
                                        <p>Web ID: {{$item->id}}</p>
                                    </td>
                                    <td class="order_description">
                                        <p>{{$item->description}}</p>
                                        @if(count($item->options))
                                            <ul>
                                            @foreach($item->options as $optionName => $optionValue)
                                                <li>{{$optionName}}: {{$optionValue}}</li>
                                            @endforeach
                                            </ul>
                                        @endif
                                    </td>
                                    <td class="order_price">
                                        <p>${{$item->price}}</p>
                                    </td>
                                    <td class="order_quantity">
                                        <p>{{$item->qty}}</p>
                                    </td>
                                    <td class="order_total">
                                        <p class="order_total_price">${{$item->subtotal}}</p>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                </div>
            </div>
        </section> <!--/#order_items-->

        <section id="do_action">
            <div class="container">
                <div class="heading">
                    <h3>Please enter your information for placing the order.</h3>
                </div>
                <div class="row">
                    <div class="form-group">
                        {!! Form::label('name', 'Your Name:') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', 'Your Email:') !!}
                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    </div>
                    @include('cart._cartTotal')
                    <div class="text-right">
                        {!! Form::submit('Place Order', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
            </div>
        </section><!--/#do_action-->
        {!! Form::close() !!}

    @else
        <p class="text-center">You have no items in the shopping cart</p>
    @endif

    @include('errors.list')
@endsection