@extends('app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Product Attribute List
                    </div>
                    {!! Form::open(['url' => 'attributes/destroy', 'method' => 'DELETE', 'action'=>['AttributeController@destroy']]) !!}
                        <div class="panel-body">
                            @foreach($attributes as $attribute)
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="checkbox" class="check" name="attribute_id[{{$attribute->id}}]" value="{{$attribute->id}}">
                                </div>
                                <div class="col-md-10">
                                    <a href="{{ action('AttributeController@edit', [$attribute->id]) }}">{{ $attribute->name }}</a>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="checkbox">
                                        <label>
                                            <input id="select-all" type="checkbox"> Select All
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                                    <a class="btn btn-default" role="button" href="/attributes/create">Add New</a>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}

                    <script>
                        $("#select-all").click(function () {
                            $(".check").prop('checked', $(this).prop('checked'));
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
@stop