<div class="form-group">
    {!! Form::label('name', 'Attribute Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('value_list', 'Values:') !!}
    {!! Form::select('value_list[]', $attributeValues, null, ['class'=>'form-control', 'id'=>'value_list', 'multiple']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>

@section('footer')
    <script type="text/javascript">
        $('#value_list').select2({
            placeholder: 'Choose Or Add Values',
            tags: true
        });
    </script>
@endsection