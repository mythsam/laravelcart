@extends('app')

@section('content')

    <h1>Edit Attribute: {{$attribute->name}}</h1>

    <hr>

    {!! Form::model($attribute, ['method' => 'PATCH', 'action'=>['AttributeController@update', $attribute->id]]) !!}

        @include('attributes._form', ['submitButtonText' => 'Edit Attribute'])

    {!! Form::close() !!}

    @include ('errors.list')
@stop