@extends('app')

@section('content')

    <h1>Add New Attribute</h1>

    <hr>

    {!! Form::open(['url' => 'attributes']) !!}

        @include('attributes._form', ['submitButtonText' => 'Add Attribute'])

    {!! Form::close() !!}

    @include('errors.list')
@stop