@extends('app')

@section('content')
    <h1>Shopping Cart</h1>

    <hr>

    {!! Form::open(['url' => 'cart', 'method' => 'PATCH', 'action'=>['CartController@update']]) !!}
    <section id="cart_items">
        <div class="container">
            <div class="table-responsive cart_info">
                @if(count($cart))
                    <table class="table table-condensed">
                        <thead>
                        <tr class="cart_menu">
                            <td class="title">Item</td>
                            <td class="description"></td>
                            <td class="price">Price</td>
                            <td class="quantity">Quantity</td>
                            <td class="total">Total</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($cart as $rowId => $item)
                            <tr>
                                <td class="cart_product">
                                    <h4><a href="products/{{$item->id}}">{{$item->name}}</a></h4>
                                    <p>Web ID: {{$item->id}}</p>
                                </td>
                                <td class="cart_description">
                                    <p>{{$item->description}}</p>
                                    @if(count($item->options))
                                        <ul>
                                            @foreach($item->options as $optionName => $optionValue)
                                                <li>{{$optionName}}: {{$optionValue}}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </td>
                                <td class="cart_price">
                                    <p>${{$item->price}}</p>
                                </td>
                                <td class="cart_quantity">
                                    <div class="cart_quantity_button">
                                        <input class="cart_quantity_input" type="text" name="quantity[{{$rowId}}]" value="{{$item->qty}}" autocomplete="off" size="2">
                                    </div>
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">${{$item->subtotal}}</p>
                                </td>
                                <td class="cart_item_delete">
                                    <div><a class="cart_item_delete_button btn btn-link" href="cart/delete/{{$rowId}}">Delete</a></div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <p class="text-center">You have no items in the shopping cart</p>
                @endif
            </div>
        </div>
    </section> <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    @include('cart._cartTotal')
                    <div class="text-right">
                        <a class="cart_destroy btn btn-default update" href="{{url('cart/destroy')}}">Clear Cart</a>
                        {!! Form::submit('Update', ['class' => 'btn btn-default update']) !!}
                        <a class="checkout btn btn-primary" href="{{url('/order')}}">Check Out</a>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#do_action-->
    {!! Form::close() !!}
@stop

@section('footer')
    <script type="text/javascript">
        $('.cart_item_delete_button').click(function (){
            return confirm('Please confirm to delete the item?')
        });

        $('.cart_destroy').click(function (){
            return confirm('Please confirm to clear the cart?')
        });
    </script>
@stop
