<div class="total_area">
    <ul class="text-right">
        <li>Discount: <span>${{$cartSummary['discount']}}</span></li>
        <li>Total: <span>${{$cartSummary['total']}}</span></li>
        <li>Grand Total: <span>${{$cartSummary['grand_total']}}</span></li>
    </ul>
</div>