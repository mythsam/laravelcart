<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Home</a>
        </div>
        <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1" aria-expanded="false" style="height: 1px;">
            <ul class="nav navbar-nav">
                @foreach($menu as $item => $url)
                <li>
                    <a href="{{$url}}"> {{$item}} </a>
                </li>
                @endforeach
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ $auth['url'] }}">@if($auth['user']) {{ $auth['user'] }}, @endif {{ $auth['text'] }}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>