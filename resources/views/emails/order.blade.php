<h3>
    Thank you {{$user['name']}}! Order received.
</h3>

<p>Order Detail as below:</p>

<section id="order_items">
    <div class="container">
        <div class="table-responsive order_info">

            <table class="table table-condensed">
                <thead>
                <tr class="order_menu">
                    <td class="title">Item</td>
                    <td class="description"></td>
                    <td class="price">Price</td>
                    <td class="quantity">Quantity</td>
                    <td class="total">Total</td>
                </tr>
                </thead>
                <tbody>

                @foreach($cart as $rowId => $item)
                    <tr>
                        <td class="order_product">
                            <h4><a href="products/{{$item->id}}">{{$item->name}}</a></h4>
                            <p>Web ID: {{$item->id}}</p>
                        </td>
                        <td class="order_description">
                            <p>{{$item->description}}</p>
                            @if(count($item->options))
                                <ul>
                                    @foreach($item->options as $optionName => $optionValue)
                                        <li>{{$optionName}}: {{$optionValue}}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </td>
                        <td class="order_price">
                            <p>${{$item->price}}</p>
                        </td>
                        <td class="order_quantity">
                            <p>{{$item->qty}}</p>
                        </td>
                        <td class="order_total">
                            <p class="order_total_price">${{$item->subtotal}}</p>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>

<section id="do_action">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                @include('cart._cartTotal')
            </div>
        </div>
    </div>
</section>