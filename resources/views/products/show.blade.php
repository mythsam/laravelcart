@extends('app')

@section('content')
    <h1>{{ $product->name }}</h1>

    <product>
        <div class="description">{{ $product->description }}</div>
        <div class="price">Price ${{ $product->price }}</div>
    </product>

    {!! Form::open(['url' => 'cart', 'method'=>'POST', 'action'=>['CartController@update']]) !!}
        {!! Form::hidden('product_id', $product->id) !!}

        <h5>Available Options:</h5>
        <ul>
            @foreach($product->attributes as $attribute)
                <li>
                    {!! Form::label($attribute->name, $attribute->name . ':') !!}
                    {!! Form::select("attribute[$attribute->name]", $attribute->values->lists('value', 'value')) !!}
                </li>
            @endforeach
        </ul>

        <div class="form-group">
            {!! Form::submit('Add To Cart', ['class' => 'btn btn-primary form-control']) !!}
        </div>

    {!! Form::close() !!}

    @if(count($bundles))
    <hr>

    <h3>Bundle other products:</h3>
    <ul class="show_style">
        @foreach($bundles as $bundle)
            <li>
                <h4>"{{$bundle->name}}".</h4>
                <i>Purchase together with products below to receive {{$bundle->discount}}% discount</i>
                <ul>
                @foreach($bundle->products()->get() as $bundledProduct)
                    @if($bundledProduct->id != $product->id)
                        <li><a href="/products/{{$bundledProduct->id}}">{{$bundledProduct->name}}</a></li>
                    @endif
                @endforeach
                </ul>
            </li>
        @endforeach
    </ul>
    @endif
@stop