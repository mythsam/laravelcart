@extends('app')

@section('content')

    <h1>Add New Product</h1>

    <hr>

    {!! Form::open(['url' => 'products']) !!}

        @include('products._form', ['submitButtonText' => 'Add Product'])

    {!! Form::close() !!}

    @include('errors.list')
@stop