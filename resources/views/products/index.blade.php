@extends('app')

@section('content')
    <h1>Product List</h1>

    @foreach($products as $product)
        <product>
            <h2>
                <a href="{{ action('ProductController@show', [$product->id]) }}">{{ $product->name }}</a>
            </h2>
            <div class="description">{{ $product->description }}</div>
            <div class="price">Price: ${{ $product->price }}</div>
        </product>
    @endforeach
@stop