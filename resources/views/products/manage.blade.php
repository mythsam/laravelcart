@extends('app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Product List
                    </div>
                    <div class="panel-body">
                        @foreach($products as $product)
                            <div class="row">
                                <div class="col-md-2">
                                    {!! Form::model($product, ['method' => 'DELETE', 'action'=>['ProductController@destroy', $product->id]]) !!}
                                    <button type="submit" class="product_destroy btn btn-link">Delete</button>
                                    {!! Form::close() !!}
                                </div>
                                <div class="col-md-10">
                                    <a href="{{ action('ProductController@edit', [$product->id]) }}">{{ $product->name }}</a> @if(!$product->active) <span>(InActive) @endif</span>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-10">
                                <a class="btn btn-default" role="button" href="/products/create">Add New</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <script type="text/javascript">
        $('.product_destroy').click(function (){
            return confirm('Please confirm to delete this product?')
        });
    </script>
@stop