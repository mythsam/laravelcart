<div class="form-group">
    {!! Form::label('name', 'Product Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'Product Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    {!! Form::checkbox('active', 1) !!}
</div>

<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <div class="input-group">
        <span class="input-group-addon">$</span>
        {!! Form::text('price', null, ['class' => 'currency form-control', 'placeholder' => 'Price in decimals']) !!}
    </div>

</div>


<div class="form-group">
    {!! Form::label('attribute_list', 'Attributes:') !!}
    {!! Form::select('attribute_list[]', $attributes, null, ['class'=>'form-control', 'id'=>'attribute_list', 'multiple']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>

@section('footer')
    <script type="text/javascript">
        $('#attribute_list').select2({
            placeholder: 'Choose the attributes'
        });
    </script>
@endsection