@extends('app')

@section('content')

    <h1>Edit Product: {{$product->name}}</h1>

    <hr>

    {!! Form::model($product, ['method' => 'PATCH', 'action'=>['ProductController@update', $product->id]]) !!}

        @include('products._form', ['submitButtonText' => 'Edit Product'])

    {!! Form::close() !!}

    @include ('errors.list')
@stop