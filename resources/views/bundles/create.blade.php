@extends('app')

@section('content')

    <h1>Add New Bundle</h1>

    <hr>

    {!! Form::open(['url' => 'bundles']) !!}

        @include('bundles._form', ['submitButtonText' => 'Add Bundle'])

    {!! Form::close() !!}

    @include('errors.list')
@stop