<div class="form-group">
    {!! Form::label('name', 'Bundle Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    {!! Form::checkbox('active', 1) !!}
</div>

<div class="form-group">
    {!! Form::label('discount', 'Discount:') !!}
    <div class="input-group">
        {!! Form::text('discount', null, ['placeholder'=>'Discount', 'class' => 'form-control']) !!}
        <div class="input-group-addon">%</div>
    </div>
</div>


<div class="form-group">
    {!! Form::label('product_list', 'Products:') !!}
    {!! Form::select('product_list[]', $products, null, ['class'=>'form-control', 'id'=>'product_list', 'multiple']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>

@section('footer')
    <script type="text/javascript">
        $('#product_list').select2({
            placeholder: 'Choose Products',
            minimumSelectionLength: 2
        });
    </script>
@endsection