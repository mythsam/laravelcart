@extends('app')

@section('content')

    <h1>Edit Bundle: {{$bundle->name}}</h1>

    <hr>

    {!! Form::model($bundle, ['method' => 'PATCH', 'action'=>['BundleController@update', $bundle->id]]) !!}

        @include('bundles._form', ['submitButtonText' => 'Edit Bundle'])

    {!! Form::close() !!}

    @include ('errors.list')
@stop