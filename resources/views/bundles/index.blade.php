@extends('app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Product Bundle List
                    </div>
                    {!! Form::open(['url' => 'bundles/destroy', 'method' => 'Delete', 'action'=>['BundleController@destroy']]) !!}
                        <div class="panel-body">
                            @foreach($bundles as $bundle)
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="checkbox" class="check" name="bundle_id[{{$bundle->id}}]" value="{{$bundle->id}}">
                                </div>
                                <div class="col-md-10">
                                    <a href="{{ action('BundleController@edit', [$bundle->id]) }}">{{ $bundle->name }}</a> <span class="small">(Discount: {{ $bundle->discount }}%)</span>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="checkbox">
                                        <label>
                                            <input id="select-all" type="checkbox"> Select All
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                                    <a class="btn btn-default" role="button" href="/bundles/create">Add New</a>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}

                    <script>
                        $("#select-all").click(function () {
                            $(".check").prop('checked', $(this).prop('checked'));
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
@stop