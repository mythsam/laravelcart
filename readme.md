## Laravel ShoppingCart Project

### To Setup the Project, run the following:

- composer install
- sudo npm install (in case npm install failed, please run npm update)
- gulp
- php artisan migrate
- php artisan db:seed --class=UsersTableSeeder
- php artisan db:seed --class=ProductsTableSeeder

### Order Email is sent using Mailgun and Laravel Queue, to listen to queue:
- php artisan queue:listen

### Config .env to run
#### setup the db:
* DB_HOST=localhost
* DB_DATABASE=[your_db_name]
* DB_USERNAME=[your_db_username]
* DB_PASSWORD=[your_db_password]

#### setup the drivers:
* CACHE_DRIVER=file
* SESSION_DRIVER=file
* MAIL_DRIVER=mailgun
* QUEUE_DRIVER=redis

#### setup mailgun variable:
* MAILGUN_DOMAIN=[your_mailgun_domain]
* MAILGUN_SECRET=[your_mailgun_api_key]  e.g."key-********"


### PHP Unit Testing
#### Add the following to .env
* TESTING_DB_HOST=localhost
* TESTING_DB_DATABASE=homestead_testing
* TESTING_DB_USERNAME=[your_db_username]
* TESTING_DB_PASSWORD=[your_db_password]

Create a Database called "homestead_testing".