<?php

namespace App\Jobs;

use App\Jobs\Job;
use Gloudemans\Shoppingcart\CartCollection;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendOrderNotificationEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var array
     */
    protected $userInfo;

    /**
     * @var CartCollection
     */
    protected $cart;

    /**
     * @var array
     */
    protected $cartSummary;

    /**
     * Create a new job instance.
     *
     * @param array $userInfo
     * @param CartCollection $cart
     * @param array $cartSummary
     */
    public function __construct(array $userInfo, CartCollection $cart, array $cartSummary)
    {
        $this->userInfo    = $userInfo;
        $this->cart        = $cart;
        $this->cartSummary = $cartSummary;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send(
            'emails.order',
            ['user' => $this->userInfo, 'cart' => $this->cart, 'cartSummary' => $this->cartSummary],
            function ($message) {
                $message->from('order@laravelproject.com', "Sam's App");

                $message->to('samzhong@hotmail.com', 'LaravelCart Owner')->subject(
                    'Order from ' . $this->userInfo['name'] . ' has received.'
                );
            }
        );
    }
}
