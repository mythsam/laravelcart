<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeNavigationMenu();
        $this->composeNavigationAuthentication();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Composer the navigation menu according to authentication
     */
    private function composeNavigationMenu()
    {
        view()->composer(
            '_nav',
            function ($view) {
                $menu = [
                    'Products' => '/products',
                    'Cart' => '/cart',
                ];
                if (Auth::user()) {
                    $menu['ProductManager'] = '/products/manage';
                    $menu['ProductAttributes'] = '/attributes';
                    $menu['Bundles'] = '/bundles';
                }

                $view->with('menu', $menu);
            }
        );
    }

    /**
     * Compose the navigation authentication section
     */
    private function composeNavigationAuthentication()
    {
        view()->composer(
            '_nav',
            function ($view) {
                $auth = ['user' => '', 'text' => 'Admin Login', 'url' => '/auth/login'];
                if (Auth::user()) {
                    $auth['user'] = Auth::user()->name;
                    $auth['text'] = 'Logout';
                    $auth['url']  = '/auth/logout';
                }

                $view->with('auth', $auth);
            }
        );
    }
}
