<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'active',
        'price'
    ];

    /**
     * Get the attributes associated with Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class);
    }

    /**
     * Get the bundles associated with Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function bundles()
    {
        return $this->belongsToMany(Bundle::class);
    }

    /**
     * Get a list of attribute ids associated with the current product
     *
     * @return array
     */
    public function getAttributeListAttribute()
    {
        return $this->attributes()->lists('attribute_id')->toArray();
    }
}
