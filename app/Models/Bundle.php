<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bundle extends Model
{
    /**
     * @var string
     */
    protected $table = 'bundles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'active',
        'discount',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * Get a list of attribute ids associated with the current product
     *
     * @return array
     */
    public function getProductListAttribute()
    {
        return $this->products()->lists('product_id')->toArray();
    }
}
