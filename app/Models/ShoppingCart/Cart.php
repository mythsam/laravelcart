<?php

namespace App\Models\ShoppingCart;

use App\Models\Bundle;

class Cart extends \Gloudemans\Shoppingcart\Cart
{

    /**
     * Find discount from matching cart item with bundle item.
     * Please note that bundle discount can only be applied once.
     *
     * @return int|number
     */
    public function discount()
    {
        $bundles      = Bundle::all();
        $cartProducts = $this->getProductsInfo();
        $discount     = 0;

        //loop through bundles
        foreach ($bundles as $bundle) {
            $bundledProducts = $bundle->products()->get()->lists('price', 'id')->toArray();
            if (count(array_intersect_key($cartProducts, $bundledProducts)) == count($bundledProducts)) {
                //calculate the price of all products in the bundle * discount %
                $discount += array_sum($bundledProducts) * ($bundle->discount / 100);

                //take out discounted product from $cartProducts holder
                foreach (array_intersect_key($cartProducts, $bundledProducts) as $key => $value) {
                    $cartProducts[$key]--;

                    if ($cartProducts[$key] <= 0) {
                        unset($cartProducts[$key]);
                    }
                }
            }
        }

        return number_format($discount, 2, '.', '');
    }

    /**
     * @return array
     */
    public function summary()
    {
        $total      = $this->total();
        $discount   = $this->discount();
        $grandTotal = $total - $discount;

        return [
            'total'       => $total,
            'discount'    => $discount,
            'grand_total' => $grandTotal
        ];
    }

    /**
     * Return an array containing product Id and count of occurrence
     * e.g. [ 1 => 2]  means product id 1 appear twice in the cart
     *
     * @return array $productInfo
     */
    private function getProductsInfo()
    {
        $cartItems = $this->content();

        $productInfo = [];
        foreach ($cartItems as $item) {
            if (!isset($productInfo[$item->id])) {
                $productInfo[$item->id] = 0;
            }
            $productInfo[$item->id]++;
        }

        return $productInfo;
    }
}
