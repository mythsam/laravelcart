<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

//Product Controller
Route::get('/products/manage', 'ProductController@manage');
Route::resource('products', 'ProductController');

//Attributes Controller
Route::resource('attributes', 'AttributeController');

//Bundle Controller
Route::resource('bundles', 'BundleController');

//Cart Controller
Route::get('/cart', 'CartController@show');
Route::get('/cart/destroy', 'CartController@destroy');
Route::post('/cart', 'CartController@add');
Route::post('/cart/bundle', 'CartController@addBundle');
Route::patch('/cart', 'CartController@update');
Route::get('/cart/delete/{id}', 'CartController@delete');

//Order Controller
Route::get('/order', 'OrderController@show');
Route::post('/order', 'OrderController@create');

//Admin Controller
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);