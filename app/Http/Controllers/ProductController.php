<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use App\Models\Product;
use App\Http\Requests;
use App\Http\Requests\ProductRequest;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var Attribute
     */
    private $attribute;

    public function __construct(Product $product, Attribute $attribute)
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->product   = $product;
        $this->attribute = $attribute;
    }

    /**
 * Display a listing of the product.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        $products = $this->product->where('active', 1)->orderBy('name')->get();

        return view('products.index')->with('products', $products);
    }

    /**
     * Display a listing of the product for admin manager.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
        $products = $this->product->orderBy('name')->get();

        return view('products.manage')->with('products', $products);
    }

    /**
     * Display the specified product.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $bundles = $product->bundles()->get();
        return view('products.show')->with(['product' => $product, 'bundles' => $bundles]);
    }

    /**
     * Display the product create view
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $attributes = $this->attribute->lists('name', 'id');

        return view('products.create')->with(['attributes' => $attributes]);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $attributes = $this->attribute->lists('name', 'id');

        return view('products.edit')->with(['product' => $product, 'attributes' => $attributes]);
    }

    /**
     * @param ProductRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ProductRequest $request)
    {
        $product = $this->product->create($request->all());

        $product->attributes()->attach($request->input('attribute_list'));

        return redirect('products/manage')->with(
            [
                'flash_message' => 'Product has been created.',
            ]
        );
    }

    /**
     * @param Product $product
     * @param ProductRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Product $product, ProductRequest $request)
    {
        $product->update($request->all());

        $product->attributes()->sync($request->input('attribute_list'));

        return redirect('products/manage')->with(
            [
                'flash_message' => 'Product ' . $product->name . ' has been updated.',
            ]
        );
    }

    /**
     * Delete the product
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Product $product)
    {
        $productName = $product->name;
        $product->delete();

        return redirect('products/manage')->with(
            [
                'flash_message' => 'Product ' . $productName . ' has been deleted.',
            ]
        );
    }
}
