<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Jobs\SendOrderEmail;
use App\Jobs\SendOrderNotificationEmail;
use App\Models\ShoppingCart\Facades\Cart;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(OrderRequest $request)
    {
        $userInfo['name']  = $request->get('name');
        $userInfo['email'] = $request->get('email');

        Cart::instance('shopping');
        $cart        = Cart::content();
        $cartSummary = Cart::summary();

        //send one copy to user
        $this->dispatch(new SendOrderEmail($userInfo, $cart, $cartSummary));

        //send email notification to me
        $this->dispatch(new SendOrderNotificationEmail($userInfo, $cart, $cartSummary));

        Cart::destroy();

        return view('orders.create')->with('user', $userInfo);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        Cart::instance('shopping');
        $cart        = Cart::content();
        $cartSummary = Cart::summary();

        return view('orders.show')->with(['cart' => $cart, 'cartSummary' => $cartSummary]);
    }
}
