<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Http\Requests;
use App\Http\Requests\AttributeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attributes = Attribute::all();

        return view('attributes.index')->with('attributes', $attributes);
    }

    public function create()
    {
        $attributeValues = AttributeValue::lists('value', 'id');

        return view('attributes.create')->with(['attributeValues' => $attributeValues]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attribute = Attribute::findOrFail($id);

        $attributeValues = AttributeValue::lists('value', 'id');

        return view('attributes.edit')->with(['attribute' => $attribute, 'attributeValues' => $attributeValues]);
    }

    /**
     * @param AttributeRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(AttributeRequest $request)
    {
        $attribute = Attribute::create($request->all());

        $attributeValues = $this->findExistsOrNew($attribute, $request);

        $attribute->values()->saveMany($attributeValues);

        return redirect('attributes')->with(
            [
                'flash_message' => 'Attribute has been created.',
            ]
        );
    }

    /**
     * @param int $id
     * @param AttributeRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, AttributeRequest $request)
    {
        $attribute = Attribute::findOrFail($id);

        $attribute->update($request->all());

        $attributeValues = $this->findExistsOrNew($attribute, $request);

        //remove old links
        $attribute->values()->delete();

        //recreate constraints
        $attribute->values()->saveMany($attributeValues);

        return redirect('attributes')->with(
            [
                'flash_message' => 'Attribute ' . $attribute->name . ' has been updated.',
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Attribute::destroy($request->input('attribute_id'));

        return redirect('attributes')->with(
            [
                'flash_message' => 'Attributes has been removed.',
            ]
        );
    }

    /**
     * @param Model $attribute
     * @param Request $request
     */
    protected function findExistsOrNew(Model $attribute, Request $request)
    {
        $attributeValues = [];
        foreach ($request->input('value_list') as $value) {
            if (AttributeValue::find($value)) {
                $attributeValues[] = new AttributeValue(
                    ['attribute_id' => $attribute->id, 'value' => AttributeValue::find($value)->value]
                );
                continue;
            }
            $attributeValues[] = new AttributeValue(['attribute_id' => $attribute->id, 'value' => $value]);
        }

        return $attributeValues;
    }
}
