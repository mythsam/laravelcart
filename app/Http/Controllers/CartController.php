<?php

namespace App\Http\Controllers;

use App\Models\ShoppingCart\Facades\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        Cart::instance('shopping');
        $cart        = Cart::content();
        $cartSummary = Cart::summary();

        return view('cart.show')->with(['cart' => $cart, 'cartSummary' => $cartSummary]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        $product_id = $request->get('product_id');
        $product = Product::find($product_id);

        $cart = Cart::instance('shopping');
        $cart->add(
            [
                'id'          => $product_id,
                'name'        => $product->name,
                'description' => $product->description,
                'qty'         => 1,
                'price'       => $product->price,
                'options'     => $request->get('attribute')
            ]
        );

        return redirect('cart');
    }

    /**
     * Update cart items
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        Cart::instance('shopping');

        foreach ($request->get('quantity') as $itemKey => $quantity) {
            Cart::update($itemKey, $quantity);
        }

        return redirect('cart');
    }

    /**
     * Remove one cart item
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Cart::instance('shopping');
        Cart::remove($id);

        return redirect('cart');
    }

    /**
     * Reset the cart
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy()
    {
        Cart::instance('shopping');
        Cart::destroy();

        return redirect('cart');
    }
}
