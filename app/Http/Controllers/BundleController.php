<?php

namespace App\Http\Controllers;

use App\Models\Bundle;
use App\Models\Product;
use App\Http\Requests;
use App\Http\Requests\BundleRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BundleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bundles = Bundle::all();

        return view('bundles.index')->with('bundles', $bundles);
    }

    public function create()
    {
        $products = Product::lists('name', 'id');

        return view('bundles.create')->with(['products' => $products]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bundle = Bundle::findOrFail($id);

        $products = Product::where('active', 1)->lists('name', 'id');

        return view('bundles.edit')->with(['bundle' => $bundle, 'products' => $products]);
    }

    /**
     * @param BundleRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(BundleRequest $request)
    {
        $bundle = Bundle::create($request->all());

        $bundle->products()->attach($request->input('product_list'));

        return redirect('bundles')->with(
            [
                'flash_message' => 'Bundle has been created.',
            ]
        );
    }

    /**
     * @param int $id
     * @param BundleRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, BundleRequest $request)
    {
        $bundle = Bundle::findOrFail($id);

        $bundle->update($request->all());
        $bundle->products()->sync($request->input('product_list'));

        return redirect('bundles')->with(
            [
                'flash_message' => 'Bundle ' . $bundle->name . ' has been updated.',
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Bundle::destroy($request->input('bundle_id'));

        return redirect('bundles')->with(
            [
                'flash_message' => 'Bundles has been removed.',
            ]
        );
    }
}
