<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BundleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|unique:bundles,name,' . $this->get('id'),
            'discount' => 'required|Integer|Min:1|Max:100',
            'product_list' => 'required|min:2'
        ];
    }
}
