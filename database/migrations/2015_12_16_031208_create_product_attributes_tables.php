<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAttributesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'attributes',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->timestamps();
            }
        );

        Schema::create(
            'attributes_value',
            function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedInteger('attribute_id')->index();
                $table->foreign('attribute_id')
                    ->references('id')
                    ->on('attributes')
                    ->onDelete('cascade');

                $table->string('value');
                $table->timestamps();
            }
        );

        Schema::create(
            'attribute_product',
            function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedInteger('attribute_id')
                    ->index();
                $table->foreign('attribute_id')
                    ->references('id')
                    ->on('attributes')
                    ->onDelete('cascade');

                $table->unsignedInteger('product_id')
                    ->index();
                $table->foreign('product_id')
                    ->references('id')
                    ->on('products')
                    ->onDelete('cascade');

                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attribute_product');
        Schema::drop('attributes_value');
        Schema::drop('attributes');
    }
}
