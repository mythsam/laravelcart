<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'bundles',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->boolean('active');
                $table->string('discount');
                $table->timestamps();
            }
        );

        Schema::create(
            'bundle_product',
            function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedInteger('bundle_id')
                    ->index();
                $table->foreign('bundle_id')
                    ->references('id')
                    ->on('bundles')
                    ->onDelete('cascade');

                $table->unsignedInteger('product_id')
                    ->index();
                $table->foreign('product_id')
                    ->references('id')
                    ->on('products')
                    ->onDelete('cascade');

                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bundle_product');
        Schema::drop('bundles');
    }
}
