<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Attribute;
use App\Models\Bundle;
use App\Models\Product;
use App\User;

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word(10),
        'description' => $faker->sentence,
        'active' => 1,
        'price' => $faker->randomFloat(2, 10, 200),
    ];
});

$factory->define(Attribute::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word(20)
    ];
});

$factory->define(Bundle::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word(20),
        'active' => 1,
        'discount' => $faker->randomNumber(2),
    ];
});