<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds for Users.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Sam Test',
            'email' => 'samzhong@hotmail.com',
            'password' => bcrypt('123123'),
        ]);

        User::create([
            'name' => 'Sean Test',
            'email' => 'sean.test@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
