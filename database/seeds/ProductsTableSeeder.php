<?php

use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds for Products.
     *
     * @return void
     */
    public function run()
    {
        //create color attribute
        $attr1 = Attribute::create(['name' => 'Color']);
        AttributeValue::create(['attribute_id'=>$attr1->id, 'value' => 'Blue']);
        AttributeValue::create(['attribute_id'=>$attr1->id, 'value' => 'Red']);
        AttributeValue::create(['attribute_id'=>$attr1->id, 'value' => 'Yellow']);

        //create size attribute
        $attr2 = Attribute::create(['name' => 'Size']);
        AttributeValue::create(['attribute_id'=>$attr2->id, 'value' => 'Small']);
        AttributeValue::create(['attribute_id'=>$attr2->id, 'value' => 'Medium']);
        AttributeValue::create(['attribute_id'=>$attr2->id, 'value' => 'Large']);

        //creaet random 10 products and link with attributes
        factory(Product::class, 10)
            ->create()
            ->each(function($attributes) use ($attr1, $attr2) {
                $attributes->attributes()->save($attr1);
                $attributes->attributes()->save($attr2);
            });
    }
}
